package Models

type Preferences struct {
	TagsID Tags `json: "TagsID", gorm:"association_foreignkey:ID"`
	UsersID Users `json: "UsersID", gorm:"association_foreignkey:ID"`
}