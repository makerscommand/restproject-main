package Models

type Tags struct {
	ID int64 `gorm:"primary_key"`
	Name string `json: "Name"`
}