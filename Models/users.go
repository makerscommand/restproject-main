package Models

type Users struct {
	ID int64 `gorm:"primary_key"`
	Login string `json: "Login"`
	Password string `json: "Password"`
	Token string `json: "Token"`
	Code int64 `json: "Code"`
}