package Models

type Receipt_dish struct {
	ReceiptID Receipt `json: "ReceiptID", gorm:"association_foreignkey:ID"`
	DishID Dish `json: "DishID", gorm:"association_foreignkey:ID"`
}