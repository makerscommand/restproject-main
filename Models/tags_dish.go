package Models

type Tags_Dish struct {
	TagsID Tags `json: "TagsID", gorm:"association_foreignkey:ID"`
	DishID Dish `json: "DishID", gorm:"association_foreignkey:ID"`
}