package Models

type Dish struct {
	ID         uint     `json: "ID", gorm:"primary_key"`
	CategoryID Category `json: "CategoryID", gorm:"association_foreignkey:ID"`
}
