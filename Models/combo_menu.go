package Models

type Combo_menu struct {
	DishOneID Dish `json: "DishOneID", gorm:"association_foreignkey:ID"`
	DishTwoID Dish `json: "DishTwoID", gorm:"association_foreignkey:ID"`
}
