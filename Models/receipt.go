package Models

type Receipt struct {
	ID         uint     `json: "ID", gorm:"primary_key"`
	ClientID Users `json: "ClientID", gorm:"association_foreignkey:ID"`
	WaiterID Users `json: "WaiterID", gorm:"association_foreignkey:ID"`
	Date date `json: "Date"`
	Price price `json: "Price"`
}