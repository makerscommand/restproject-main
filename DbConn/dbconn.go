package DbConn

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
)



var DB *gorm.DB

//DB constants
const (
	host     = "localhost"
	port     = 5432
	user     = "egor"
	password = ""
	dbname   = "restprojectdb"
)

func MakeConnectionToDB() *gorm.DB {
	db, err := gorm.Open("postgres", "host=localhost port=5432 user=egor dbname=restprojectdb sslmode=disable password=SA")
	if err != nil {
		log.Fatal("failed to connect database")
	}
	return db
}

