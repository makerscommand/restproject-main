package main

import (
	"net/http"
	"log"
	"restproject-main/Controllers"
	"restproject-main/DbConn"
)

func main()  {
	DbConn.DB = DbConn.MakeConnectionToDB()
	defer DbConn.DB.Close()


	http.HandleFunc("/get_all_categories", Controllers.GetAllCategoriesHandler())
	http.HandleFunc("/get_all_tags", Controllers.GetAllTagsHandler())
	http.HandleFunc("/get_all_tags", Controllers.GetAllUsersHandler())


	http.Handle("/", http.FileServer(http.Dir("WebClient")))
	log.Println("server started")
	err := http.ListenAndServe(":4568", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
