package Controllers

import (
	"log"
	"net/http"
	"restproject-main/Models"
	"encoding/json"
	"restproject-main/Utils"
	"restproject-main/DbConn"
	"restproject-main/Controllers/Common"
)

func GetAllCategoriesHandler() http.HandlerFunc {
	var categories []Models.Category
	DbConn.DB.Find(&categories)
	log.Println("GetAllCategoriesHandler(): categories: ", categories)
	return Common.GetDataHandler(categories)
}

func AddNewCategoryHandler() http.HandlerFunc {
	return func(writer http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)
		var tag Models.Category
		err := decoder.Decode(&tag)
		Utils.CheckErr(err)
		defer r.Body.Close()

	}
}