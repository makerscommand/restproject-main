package Common

import (
	"net/http"
	"encoding/json"
	"restproject-main/Utils"
)

//---HANDLERS---
func GetDataHandler(data interface{}) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		serializedDataToSend, err := json.Marshal(data)
		Utils.CheckErr(err)
		w.Write(serializedDataToSend)
	}
}
