package Controllers

import (
	"log"
	"net/http"
	"RestProjectMain/Models"
	"encoding/json"
	"RestProjectMain/Utils"
	"RestProjectMain/DbConn"
	"RestProjectMain/Controllers/Common"
)

func GetAllTagsHandler() http.HandlerFunc {
	var tags []Models.Category
	DbConn.DB.Find(&tags)
	log.Println("GetAllTagsHandler(): tags: ", tags)
	return Common.GetDataHandler(tags)
}

func AddNewTagHandler() http.HandlerFunc {
	return func(writer http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)
		var category Models.Tags
		err := decoder.Decode(&category)
		Utils.CheckErr(err)
		defer r.Body.Close()

	}
}
