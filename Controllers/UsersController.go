package Controllers

import (
	"log"
	"net/http"
	"RestProjectMain/Models"
	"encoding/json"
	"RestProjectMain/Utils"
	"RestProjectMain/DbConn"
	"RestProjectMain/Controllers/Common"
)

func GetAllUsersHandler() http.HandlerFunc {
	var users []Models.Category
	DbConn.DB.Find(&users)
	log.Println("GetAllUsersHandler(): users: ", users)
	return Common.GetDataHandler(users)
}

func AddNewUserHandler() http.HandlerFunc {
	return func(writer http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)
		var user Models.Users
		err := decoder.Decode(&user)
		Utils.CheckErr(err)
		defer r.Body.Close()

	}
}
