import Vue from 'vue'
import App from './App.vue'
import store from './store/index'
import router from './router/index'
import { sync } from 'vuex-router-sync'
import ElementUI from 'element-ui';
Vue.use(ElementUI);


sync(store, router)


new Vue({
  el: '#app',
  template: '<App/>',
  components: { App },
  router,
  store
})
