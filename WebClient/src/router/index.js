import Vue from 'vue'
import Router from 'vue-router'
import MainBlock from '../components/MainBlock'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: MainBlock
    }
  ]
})
