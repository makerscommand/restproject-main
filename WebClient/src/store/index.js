import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    results: [],
    user: {},
    institution: {},
    dishes: {},
    categories: [ 'Напитки', 'Супы', 'Десерты' ]
  },
  getters: {
    results(state) {
      let results = state.results

      return results.map(item => {
        item.url = 'https://ru.wikipedia.org/wiki/' + item.title
        return item
      })
    },
    user(state) {
      return {
        name: 'Ivan',
        surname: 'Ivanov',
        institution: 'Рога и копыта'
      }
    },
    institution(state) {
      return {
        name: 'Рога и копыта'
      }
    },
    categories(state) {
      return state.categories
    },
    currentDishesList(state) {
      console.info("currentDishesList: ", state);
      return state.dishes
    }
  },

  mutations: {
    set(state, { type, items }) {
      state[type] = items
    },
    newCategory(state, { type, items }) {
      console.info("item: ", items);
      state[type].push(items)
    },
    setDishes(state, {type, items}) {
      console.info('setDishes: ', items);
      state[type] = items;
    }
  },

  actions: {
    search({ commit }, query) {
    },

    addNewCategory({ dispatch, commit }, data) {
      //тут должен быть запрос
      //при удачном ответе делать коммит
      console.info("data: ", data);
      commit('newCategory', { type: 'categories', items: data});
      console.info("addNewCategory");
    },

    dishesByCategory({ commit }, categoryIndex) {
      console.info('categoryIndex: ', categoryIndex);
      //запрос
      let tempDishes = {};
      if (categoryIndex == '0') {
        tempDishes = [{
          name: 'Капучино',
          price: '150',
          payloadInfo: '140мл',
          description: 'Кофейный напиток итальянской кухни на основе эспрессо с добавлением в него подогретого вспененного молока'
        }];
      } else {
        tempDishes = [{
          name: 'Нет данных',
          price: '--',
          payloadInfo: '--',
          description: '--'
        }];
      }
       commit('setDishes', { type: 'dishes', items: tempDishes});
    }
  }
})

export default store
